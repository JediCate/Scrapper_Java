package automatedFramework;

import java.awt.*;

public class Product {
    private String name;
    private String code;
    private String availability;
    private String price;
    private String imagePath;
    private String description;

    public Product(String name, String code, String availability, String price, String imagePath, String description){
        this.name = name;
        this.code = code;
        this.availability = availability;
        this.price = price;
        this.imagePath = imagePath;
        this.description = description;
    }

    @Override
    public String toString() {
        return name + ";" + code + ";" + availability + ";" + price + ";" + imagePath + ";" + description + ";";
    }
}
