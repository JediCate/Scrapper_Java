package automatedFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class FirstTestCase {
    static WebDriver driver;
    static int counter;
    static String lastMotoFilePath = "D:\\last_moto.txt";
    static String lastCodeFilePath = "D:\\last_product_code.txt";
    // test gitignore
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Trompetica\\IdeaProjects\\chromedriver.exe");
       /* ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");*/

        driver = new ChromeDriver();
        Credentials credentials = new Credentials();
        driver.get(credentials.getAuthLink());
        // Enter userd id
        WebElement username = driver.findElement(By.id("login"));
        username.sendKeys(credentials.getUsername());
        //Enter Password
        WebElement password = driver.findElement(By.id("password"));
        password.sendKeys(credentials.getPassword());
        //Submit button
        username.submit();
        driver.get(credentials.getCrawlLink());
        nav();
        driver.quit();
    }


    public static void nav() {
        //get all links as webelements
        List<WebElement> links = driver.findElements(By.cssSelector("div.filterDeeper a"));
        List<String> arr = new ArrayList<String>();
        //save links in an array
        for (WebElement link : links) {
            String href = link.getAttribute("href");
            arr.add(href);
        }
        counter++;
        System.out.println(counter);
        if (links.isEmpty()) {
            processProductPage();
        } else {
            for (String href : arr) {
                //if file is not empty and the function is at it's first 4 loops
                //match href with the motorcycle in the bookmark
                if(!isFileEmpty(lastMotoFilePath)&& counter <= 4){
                    //this is the bookmark
                    String currentMoto = readFromFile(lastMotoFilePath).toLowerCase()
                            .replaceAll("([a-z])(\\d+)", "$1-$2");
                    String[] motoAdressArr = currentMoto.split("/");
                    switch (counter){
                        case 1:
                            if(!href.contains(motoAdressArr[0])){
                               continue;
                            }
                            break;
                        case 2:
                            if(!href.contains(motoAdressArr[1])){
                                continue;
                            }
                            break;
                        case 3:
                            if(!href.contains(motoAdressArr[2])){
                                continue;
                            }
                            break;
                        case 4:
                            if(!href.contains(motoAdressArr[3])){
                                continue;
                            }
                            break;
                    }
                }
                //click on the first link
                driver.get(href);
                nav();
            }
        }
    }

    public static void processProductPage(){
        String currentUrl = driver.getCurrentUrl();
        String loadAll = currentUrl + "?page=-1";
        WebElement radioCatalog = driver.findElement(By.id("change-catalog-type1"));
        if (!radioCatalog.isSelected()) {
            radioCatalog.click();
        }
        driver.get(loadAll);
        //nume moto
        String textName = driver.findElement(By.xpath("//*[@id=\"page-16\"]/div[2]/div[1]/div[1]/div[2]/div[5]")).getText();
        String motoName = textName.substring(textName.indexOf("/") + 1)
                .replaceAll("[\\s\n.]", "");
        writeToFile(motoName, lastMotoFilePath, false);
        System.out.println(motoName);
        //subcategorie
        List<WebElement> subCategorii = driver.findElements(By.cssSelector("h2.GoodsCatalogGroup"));
        List<WebElement> productsUl = driver.findElements(By.cssSelector("ul.GoodsCatalog"));
        for(int i = 0; i < subCategorii.size(); i++){
            String catName = subCategorii.get(i).getText();
            WebElement currentUl = productsUl.get(i);
            List<WebElement> productsList = currentUl.findElements(By.cssSelector("li"));
            for (WebElement product : productsList) {
                String code = product.findElement(By.cssSelector(".elements1 .stock-number a")).getText();
                writeToFile(code, lastCodeFilePath, false);
                String productName = product.findElement(By.cssSelector("h3 a")).getText();
                String availability = product.findElement(By.cssSelector("h3 div.stock-icon span")).getAttribute("title");
                String imgSource = product.findElement(By.cssSelector("a.image img")).getAttribute("src");
                String imgTitle = product.findElement(By.cssSelector("a.image img")).getAttribute("alt");
                String imagePath = "data/" + processImage(imgSource, imgTitle);
                String price = product.findElement(By.cssSelector(".elements1 .price")).getText().replaceAll(",-", "");
                String description = product.findElement(By.cssSelector(".elements2")).getAttribute("textContent").trim();

                Motorcycle motorcycle = new Motorcycle(motoName, currentUrl, catName, new Product(productName, code, availability, price, imagePath, description));
                writeToFile(motorcycle.toString(), "D:\\produse.txt", true);
                System.out.println(motorcycle.toString());
            }
        }
        //wait 1 sec before continuing
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    public static String processImage(String src, String imageTitle) {
        String newTitle = null;
        try {
            newTitle = imageTitle.toLowerCase().replaceAll("[ ]", "-").replaceAll("[()]]", "");
            src =src.substring(0, src.indexOf("?"));
            URL imageUrl = new URL(src);
            BufferedImage buffer = ImageIO.read(imageUrl);
            ImageIO.write(buffer, "png", new File("D:\\poze_motonet\\" + newTitle + ".png"));
        } catch (Exception e) {
            e.getStackTrace();
        }
        return newTitle;
    }

    public static void writeToFile(String s, String filePath, boolean append){
        BufferedWriter bufferedWriter = null;
        FileWriter fileWriter = null;
        try{
            File file = new File(filePath);
            if(!file.exists()){
                file.createNewFile();
            }
            fileWriter = new FileWriter(file.getAbsoluteFile(), append);
            bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(s);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try{
                if(bufferedWriter != null){
                    bufferedWriter.close();
                }
                if(fileWriter != null){
                    fileWriter.close();
                }
            }catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    public static String readFromFile(String path){
        File file = new File(path);
        String lastMoto = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            if((lastMoto = reader.readLine()) != null){
                System.out.println(lastMoto);
            }
            reader.close();
        }catch (IOException e) {
            System.out.println("file with bookmark not found");
            e.printStackTrace();
        }
        return lastMoto ;
    }


    public static boolean isFileEmpty(String path){
        File file = new File(path);
        if(!file.exists()){
            System.out.println("file does not exist");
            return true;
        }
        try{
            BufferedReader buffer = new BufferedReader(new FileReader(file));
            //check the first line
            if(buffer.readLine() == null){
                System.out.println("file is empty");
                return true;
            }
        }catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
