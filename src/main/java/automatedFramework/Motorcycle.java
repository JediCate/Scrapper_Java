package automatedFramework;

import java.util.ArrayList;

public class Motorcycle {
    private String motoName;
    private String currentUrl;
    private String subCategory;
    private Product product;

    public Motorcycle(String motoName, String currentUrl, String subCategory, Product product){
        this.motoName = motoName;
        this.currentUrl = currentUrl;
        this.subCategory = subCategory;
        this.product = product;
    }

    @Override
    public String toString() {
        return motoName + ";" + subCategory + ";" + product.toString();
    }
}
